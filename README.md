
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Task Script

Task assigment script.

UI frameworks: [Vue 3](https://vuejs.org/), [Inertia](https://jetstream.laravel.com/stacks/inertia.html) [TailwindCss](https://tailwindcss.com) and [Naive UI](https://www.naiveui.com/en-US/light) 

Backend : [Jetstream](https://jetstream.laravel.com/introduction.html) and [Sail](https://laravel.com/docs/10.x/sail)

To get tasks from APIs please run artisan tasks:create command

## Learning Laravel

To configure please see Laravel Sail setup [documentation](https://laravel.com/docs/10.x/sail#introduction)

# task-assignment
