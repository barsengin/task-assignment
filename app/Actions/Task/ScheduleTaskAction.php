<?php

namespace App\Actions\Task;

use App\Models\Developer;
use App\Models\DeveloperTask;
use App\Models\Task;
use mysql_xdevapi\Collection;

class ScheduleTaskAction
{
    public static array $taskFinal = [];
    public static $tasks;
    public static array $tmpDeveloperTask;
    public static float $average;
    public static function handle()
    {
        self::createTaskFinal();
        DeveloperTask::where('completed','!=',1)->update([
            'completed' => 1,
            'completed_at' => now()
        ]);
        self::$tasks = Task::all()->sortBy([
            ['value','desc'],
            ['duration','desc']
        ]);
        while(self::$tasks->count()) {
            $average = self::calculateAvg(self::$tasks);
            Developer::all()
                ->sortBy([
                    ['value','desc']
                ])
                ->each(function ($item) use ($average){
                    self::calculate(self::$tasks, $average, $item->id);
                });

            $week = 1;
            foreach (self::$taskFinal as $item) {
                if($item['estimated_duration'] >= $week) $week = $item['estimated_duration'];
            }
        }

        AssignTaskAction::handle(self::$tmpDeveloperTask);
        $developerTasks = GetDeveloperTasksAction::handle();
    }

    public static function calculateAvg($tasks)
    {
        return (integer) ceil($tasks->where('value', '=', '1')->sum('duration') +
                $tasks->where('value', '=', '2')->sum('duration')*2 +
                $tasks->where('value', '=', '3')->sum('duration')*3 +
                $tasks->where('value', '=', '4')->sum('duration')*4 +
                $tasks->where('value', '=', '5')->sum('duration')*5) / 15;
    }

    public static function createTaskFinal()
    {
        $developers = Developer::all();
        foreach ($developers as $developer) {
            self::$taskFinal[$developer->id] = [
                'tasks' => [],
                'estimated_duration' => 0
            ];
        }
    }

    public static function calculate(\Illuminate\Support\Collection $tasks, $average, int $developer)
    {
        $art = $average < 10 ? 4 : 3;
        $returnedTasks = [];
        $tmp = 0;

        //dd($value);
        foreach($tasks->toArray() as $key=>$value) {
            $es = $value['duration'];
            $esTmp = $value['duration'];
            $es = ($developer != $value['value']) ? (integer)ceil($value['value']* $es/$developer) : $value['duration'];

            if($tmp <= $average ) $less = 1 ; else break;
            $tmp = $tmp + $es;

            if($tmp >= $average + $art ) {
                $tmp = $tmp - $es;
                break;
            } else {
                self::$taskFinal[$developer]['tasks'][] = $value;
                self::$tmpDeveloperTask[$developer]['tasks'][] = [
                    'task_id' => $value['id'],
                    'developer_id' => $developer,
                    'expect_value' => ($developer!=$value['value']) ? ($value['value']* $esTmp/$developer) : $value['duration']
                ];
                $tasks->forget($key);
            }
        }
        self::$taskFinal[$developer]['estimated_duration']  += $tmp;
        return $tmp;
    }
}
