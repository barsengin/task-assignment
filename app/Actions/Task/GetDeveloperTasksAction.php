<?php

namespace App\Actions\Task;

use App\Models\Developer;
use App\Models\DeveloperTask;
use App\Models\Task;
use Illuminate\Support\Facades\Http;

class GetDeveloperTasksAction
{
    /**
     * @return mixed
     */
    public static function handle(): mixed
    {
        return DeveloperTask::groupBy('developer_id')
            ->with('developer')
            ->selectRaw('SUM(CEILING(expect_value)) as sum')
            ->selectRaw('COUNT(*) as count')
            ->selectRaw('developer_id')
            ->get()
            ->map(function ($item, $key) {
                return [
                    'sum' => $item['sum'],
                    'count' => $item['count'],
                    'developer' => $item->developer->toArray(),
                ];
            });
    }
}
