<?php

namespace App\Actions\Task;

use App\Models\Task;
use Illuminate\Support\Facades\Http;

class GetTaskFromApiAction
{
    /**
     * @param string $apiUrl
     * @param string $durationKey
     * @param string $nameKey
     * @param string $valueKey
     * @param string $providerName
     */
    public static function handle(string $apiUrl,
                                  string $durationKey,
                                  string $nameKey,
                                  string $valueKey,
                                  string $providerName)
    {
        try {
            $tasks = Http::get($apiUrl)->collect();

            $result = $tasks->map(function($item) use ($durationKey, $nameKey, $valueKey, $providerName) {
                $item['created_at'] = now();
                $item['updated_at'] = now();

                if($durationKey != 'duration') {
                    $item['duration'] = $item[$durationKey];
                    unset($item[$durationKey]);
                }
                if($nameKey != 'name') {
                    $item['name'] = $item[$nameKey];
                    unset($item[$nameKey]);
                }
                if($valueKey != 'value') {
                    $item['value'] = $item[$valueKey];
                    unset($item[$valueKey]);
                }

                $item['provider_name'] = $providerName;
                return $item;
            });
            Task::insert($result->all());
            return true;

        } catch (\Exception $e) {
            return $e;
        }
    }
}
