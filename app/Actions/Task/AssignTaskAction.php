<?php

namespace App\Actions\Task;

use App\Models\Developer;
use App\Models\DeveloperTask;
use App\Models\Task;
use Illuminate\Support\Facades\Http;

class AssignTaskAction
{
    /**
     * @param array $tasks
     * @return \Exception|true
     */
    public static function handle(array $tasks)
    {
        $developers = Developer::all()->pluck('id')->toArray();
        foreach ($developers as $developer) {
            DeveloperTask::insert($tasks[$developer]['tasks']);
        }
    }
}
