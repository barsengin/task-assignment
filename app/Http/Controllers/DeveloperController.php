<?php

namespace App\Http\Controllers;

use App\Http\Resources\DeveloperIndexResource;
use App\Http\Resources\DeveloperTaskListResource;
use App\Models\Developer;
use App\Models\DeveloperTask;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class DeveloperController extends BaseController
{
    public array $tmpDeveloperTask = [];

    public function index()
    {
        return inertia('DeveloperList',[
            'developers' => DeveloperIndexResource::collection(Developer::all())
        ]);
    }
}
