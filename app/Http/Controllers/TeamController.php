<?php

namespace App\Http\Controllers;

use App\Actions\Task\GetDeveloperTasksAction;
use App\Actions\Task\ScheduleTaskAction;
use App\Http\Resources\DeveloperTaskListResource;
use App\Models\DeveloperTask;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class TeamController extends BaseController
{
    public function index()
    {
        return inertia('FreshBlank',[
            'tasks' => Task::all()->values()->toArray()
        ]);
    }
    public function create()
    {
        ScheduleTaskAction::handle();
        $tmp = GetDeveloperTasksAction::handle();

        return inertia('WeeklySummary',[
            'tasks' =>$tmp->toArray(),
            'maxWeek' => ceil($tmp->sortByDesc('sum')->pluck('sum')->first()/45)
        ]);
    }

    public function getDeveloperTask(Request $request)
    {
        $taskList = DeveloperTask::all()->where('developer_id',$request->id);
        return inertia('DeveloperTask',[
            'tasks' => DeveloperTaskListResource::collection($taskList),
        ]);
    }
}
