<?php

namespace App\Http\Resources;

use App\Models\Developer;
use Illuminate\Http\Resources\Json\JsonResource;
/**
 * @mixin Developer
 *
 * @extends  JsonResource<Developer>
 */
class DeveloperIndexResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request):array
    {
        return [
            'id'            => $this->id,
            'name'     => $this->name,
            'value'     => $this->value,
            'tasks'     => $this->tasks
        ];
    }
}
