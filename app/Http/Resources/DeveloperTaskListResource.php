<?php

namespace App\Http\Resources;

use App\Models\DeveloperTask;
use App\Models\Referee;
use Illuminate\Http\Resources\Json\JsonResource;
/**
 * @mixin DeveloperTask
 *
 * @extends  JsonResource<DeveloperTask>
 */
class DeveloperTaskListResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request):array
    {
        return [
            'id'            => $this->id,
            'developer'     => $this->developer->name,
            'developer_value'     => $this->developer->value,
            'task_name'     => $this->task->name,
            'task_duration'     => $this->task->duration,
            'task_value'    => $this->task->value,
            'expect_duration'   => $this->expect_value,
        ];
    }
}
