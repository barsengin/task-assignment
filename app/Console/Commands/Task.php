<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Task extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task created on database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->call('firstTasks:create');
        $this->call('secondTasks:create');
    }
}
