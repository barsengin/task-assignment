<?php

namespace App\Console\Commands\Tasks;

use App\Actions\Task\GetTaskFromApiAction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SecondProvider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'secondTasks:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task created on database';
    private string $url =  'https://run.mocky.io/v3/7b0ff222-7a9c-4c54-9396-0df58e289143';
    protected string $providerName = 'Second Provider';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if(GetTaskFromApiAction::handle($this->url,'estimated_duration','id', 'value',$this->providerName))
            echo $this->url.' task download and created on database ' . PHP_EOL;
        else
            echo $this->url.' task did not downloaded and error occured... ' . PHP_EOL;
    }
}
