<?php

namespace App\Console\Commands\Tasks;

use App\Actions\Task\GetTaskFromApiAction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FirstProvicer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firstTasks:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task created on database';
    private string $url = 'https://run.mocky.io/v3/27b47d79-f382-4dee-b4fe-a0976ceda9cd';

    protected string $providerName = 'First Provider';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if(GetTaskFromApiAction::handle($this->url,'sure','id','zorluk', $this->providerName))
            echo $this->url.' task download and created on database' . PHP_EOL;
        else
            echo $this->url.' task did not downloaded and error occured... ' . PHP_EOL;
    }
}
