<?php

namespace App\Models;

use App\Models\Scopes\ActiveTask;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * @property integer $id
 * @property int  $developer_id
 * @property int  $task_id
 * @property float  $expect_value
 * @property bool  $completed
 *
 * @property-read  Task $task
 * @property-read  Developer $developer
 */
class DeveloperTask extends Model
{
    use HasFactory,SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'task_id',
        'developer_id',
        'expected_value',
        'completed',
        'completed_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime',
        'deleted_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [];

    public  $timestamps = true;

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::addGlobalScope(new ActiveTask());
    }

    /**
     * @return BelongsTo
     */
    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class,'task_id','id');
    }

    /**
     * @return BelongsTo
     */
    public function developer(): BelongsTo
    {
        return $this->belongsTo(Developer::class,'developer_id','id');
    }
}
