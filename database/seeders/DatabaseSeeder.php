<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\League;
use App\Models\Question;
use App\Models\Referee;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(1)->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'two_factor_secret' => null,
            'two_factor_recovery_codes' => null,
            'remember_token' => Str::random(10),
            'profile_photo_path' => null,
            'current_team_id' => null,
        ]);


        $leagueArr = array('Premier League','Bundesliga','Super League','La League');
        $country = array("England", "Germany", "Turkey", "Spain", "Portugal");
        /** @var League $league */
        $league = League::factory()->create([
            'display_name'  => $leagueArr[array_rand($leagueArr)],
            'country'       => 'England'
        ]);

        $teamArr = array('Arsenal', 'Chelsea','Manchester City','Liverpool','Manchester United');
        for ($i=0; $i<4; $i++) {
            $name = $teamArr[rand(0,1000)%4];
            Team::factory()->create([
                'display_name'      => $name,
                'league_id'         => $league->id,
            ]);
            $teamArr = array_diff($teamArr,array($name));
            $teamArr = array_values($teamArr);
        }

    }
}
