<?php

namespace Database\Seeders;

use App\Models\Developer;
use Illuminate\Database\Seeder;

class DeveloperSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        for ($i=1; $i<6; $i++ ) {
            Developer::factory()->create([
                'name' => 'Dev-'.$i,
                'value' => $i,
            ]);
        }
    }
}
