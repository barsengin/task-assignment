<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('developer_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('developer_id');
            $table->unsignedInteger('task_id');
            $table->float('expect_value'); // hour
            $table->float('completed_duration')->nullable(); // hour
            $table->integer('completed')->default(false);
            $table->dateTime('completed_at')->nullable();

            $table->foreign('developer_id')->references('id')->on('developers')->onUpdate('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('developer_tasks');
    }
};
